﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour {

	public string roomName = "New Room";
	public GameObject prefab;
	public List <Transform> newRoomSpawns = new List<Transform> ();

	private Level level;

	void Start () {

		level = transform.parent.GetComponent <Level> ();

		for (int i = 0; i < newRoomSpawns.Count; i++) {

			if (level.transform.childCount < 7) {
				
				if (newRoomSpawns [i].tag == "Corridor Spawn") {

					Instantiate (level.corridors [Random.Range (0, level.corridors.Count)], newRoomSpawns [i].position, newRoomSpawns [i].rotation, level.transform);
				} else if (newRoomSpawns [i].tag == "Room Spawn") {

					Instantiate (level.rooms [Random.Range (0, level.rooms.Count)], newRoomSpawns [i].position, newRoomSpawns [i].rotation, level.transform);
				}
			}
		}
	}

	void Update () {


	}
}
